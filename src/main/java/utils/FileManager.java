package utils;

import config.PropManager;
import utils.entities.Author;
import utils.entities.Book;
import utils.entities.Genre;

import java.util.List;


public class FileManager {

    private FileManager() {
    }

    public static Author getValidAuthor() {
        return FileReader.readObject(PropManager.getValidAuthorFilePath(), Author.class);
    }

    public static Author getUpdatedAuthor() {
        return FileReader.readObject(PropManager.getUpdatedAuthorFilePath(), Author.class);
    }

    public static Book getValidBook() {
        return FileReader.readObject(PropManager.getValidBookFilePath(), Book.class);
    }

    public static Genre getValidGenre() {
        return FileReader.readObject(PropManager.getValidGenreFilePath(), Genre.class);
    }

    public static List<Author> getInvalidAuthorsList() {
        return FileReader.readListOfObject(PropManager.getInvalidAuthorFilePath(), Author.class);
    }

    public static List<Author> getInvalidAuthors1List() {
        return FileReader.readListOfObject(PropManager.getInvalidAuthor1FilePath(), Author.class);
    }
}
