package utils.entities;

import java.util.Objects;

public class Book {

    private Long bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private Additional additional;
    private Long publicationYear;

    private static class Additional {
        private Long pageCount;
        private Size size;

        private static class Size {
            private Double height;
            private Double width;
            private Double length;

            public Size(Double height, Double width, Double length) {
                this.height = height;
                this.width = width;
                this.length = length;
            }

            public Size() {
            }

            public Double getHeight() {
                return height;
            }

            public Double getWidth() {
                return width;
            }

            public Double getLength() {
                return length;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Size)) return false;
                Size size = (Size) o;
                return getHeight().equals(size.getHeight()) && getWidth().equals(size.getWidth()) && getLength().equals(size.getLength());
            }

            @Override
            public int hashCode() {
                return Objects.hash(getHeight(), getWidth(), getLength());
            }

            @Override
            public String toString() {
                return "Size{" +
                        "height=" + height +
                        ", width=" + width +
                        ", length=" + length +
                        '}';
            }
        }

        public Additional(Long pageCount, Size size) {
            this.pageCount = pageCount;
            this.size = size;
        }

        public Additional() {
        }

        public Long getPageCount() {
            return pageCount;
        }

        public Size getSize() {
            return size;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Additional)) return false;
            Additional that = (Additional) o;
            return getPageCount().equals(that.getPageCount()) && getSize().equals(that.getSize());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getPageCount(), getSize());
        }

        @Override
        public String toString() {
            return "Additional{" +
                    "pageCount=" + pageCount +
                    ", size=" + size +
                    '}';
        }
    }

    public Book(Long bookId, String bookName, String bookLanguage, String bookDescription, Additional additional, Long publicationYear) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookLanguage = bookLanguage;
        this.bookDescription = bookDescription;
        this.additional = additional;
        this.publicationYear = publicationYear;
    }

    public Book() {
    }

    public Long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookLanguage() {
        return bookLanguage;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public Additional getAdditional() {
        return additional;
    }

    public Long getPublicationYear() {
        return publicationYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getBookId().equals(book.getBookId()) && getBookName().equals(book.getBookName()) && getBookLanguage().equals(book.getBookLanguage()) && getBookDescription().equals(book.getBookDescription()) && getAdditional().equals(book.getAdditional()) && getPublicationYear().equals(book.getPublicationYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBookId(), getBookName(), getBookLanguage(), getBookDescription(), getAdditional(), getPublicationYear());
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", bookLanguage='" + bookLanguage + '\'' +
                ", bookDescription='" + bookDescription + '\'' +
                ", additional=" + additional +
                ", publicationYear=" + publicationYear +
                '}';
    }
}
