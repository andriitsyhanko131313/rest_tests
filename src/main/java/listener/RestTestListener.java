package listener;

import io.qameta.allure.Attachment;
import io.restassured.RestAssured;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class RestTestListener implements ITestListener {

    private ByteArrayOutputStream outputStream;

    @Override
    public void onTestStart(ITestResult iTestResult) {
        outputStream = new ByteArrayOutputStream();
        PrintStream logStream = new PrintStream(outputStream, true);
        RestAssured.filters(new RequestLoggingFilter(LogDetail.ALL, logStream),
                new ResponseLoggingFilter(LogDetail.ALL, logStream));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        addFileToAllure(outputStream);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        addFileToAllure(outputStream);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        addFileToAllure(outputStream);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        addFileToAllure(outputStream);
    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

    private byte[] attach(ByteArrayOutputStream stream) {
        byte[] byteLogs = stream.toByteArray();
        stream.reset();
        return byteLogs;
    }

    @Attachment(value = "logs")
    public byte[] addFileToAllure(ByteArrayOutputStream stream) {
        return attach(stream);
    }
}
