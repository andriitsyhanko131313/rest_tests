package config;


public class PropManager {
    private PropManager() {
    }

    public static String getValidAuthorFilePath() {
        return PropReader.readProperties("valid_author_file_path");
    }

    public static String getBaseUrl() {
        return PropReader.readProperties("base_url");
    }

    public static String getUpdatedAuthorFilePath() {
        return PropReader.readProperties("updated_author_file_path");
    }

    public static String getValidBookFilePath() {
        return PropReader.readProperties("valid_book_file_path");
    }

    public static String getValidGenreFilePath() {
        return PropReader.readProperties("valid_genre_file_path");
    }

    public static String getInvalidAuthorFilePath() {
        return PropReader.readProperties("invalid_author_file_path");
    }
    public static String getInvalidAuthor1FilePath() {
        return PropReader.readProperties("invalid_author1_file_path");
    }
}
