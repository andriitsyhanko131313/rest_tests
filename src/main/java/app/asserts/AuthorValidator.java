package app.asserts;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import utils.entities.Author;

import java.util.Arrays;
import java.util.List;

public class AuthorValidator {

    public static final String EXPECTED_BUT_FOUND = "Expected [%s], but found [%s]";

    @Step("Verify status code]")
    public void statusNotFoundAsserter(Response response) {
        int exp = HttpStatus.SC_NOT_FOUND;
        int actual = response.statusCode();
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, actual, exp));
    }

    @Step("Verify status code")
    public void statusConflictAsserter(Response response) {
        int exp = HttpStatus.SC_CONFLICT;
        int actual = response.statusCode();
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, actual, exp));
    }

    @Step("Verify status code")
    public void statusBadRequestAsserter(Response response) {
        int exp = HttpStatus.SC_BAD_REQUEST;
        int actual = response.statusCode();
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, actual, exp));
    }

    @Step("Verify create author")
    public void createAuthorAsserter(Response response, Author exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        Author actual = response.body().as(Author.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }

    @Step("Verify get author")
    public void getAuthorAsserter(Response response, Author exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_OK);
        Author actual = response.body().as(Author.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }

    @Step("Verify delete author")
    public void deleteAuthorAsserter(Response response) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_NO_CONTENT,
                String.format(EXPECTED_BUT_FOUND,
                        response.statusCode(), HttpStatus.SC_NO_CONTENT));
    }

    @Step("Verify update author")
    public void updateAuthorAsserter(Response response, Author exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_OK);
        Author actual = response.body().as(Author.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }

    @Step("Verify get author by name")
    public void getAuthorsByNameAsserter(Response response, String authorFirstName) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_OK);
        List<Author> actual = response
                .body().jsonPath()
                .getList("$", Author.class);
        SoftAssert softAssert = new SoftAssert();
        actual.forEach(author -> softAssert
                .assertEquals(author.getAuthorName().getFirst(), authorFirstName,
                        String.format(EXPECTED_BUT_FOUND,
                                author.getAuthorName().getFirst(), authorFirstName)));
        softAssert.assertAll();
    }

    @Step("Verify get authors by genre")
    public void getAuthorsByGenreAsserter(Response response, Author... authors) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_OK);
        List<Author> actual = response
                .body().jsonPath()
                .getList("$", Author.class);
        List<Author> expected = Arrays.asList(authors);
        Assert.assertEquals(actual, expected, String.format(EXPECTED_BUT_FOUND, actual, expected));
    }
}
