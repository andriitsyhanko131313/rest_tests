package app.asserts;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import utils.entities.Author;
import utils.entities.Book;

public class BookValidator {

    public static final String EXPECTED_BUT_FOUND = "Expected [%s], but found [%s]";

    @Step("Verify create book")
    public void createBookAsserter(Response response, Book exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        Book actual = response.body().as(Book.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }

    @Step("Verify get author")
    public void getAuthorAsserter(Response response, Author exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_OK);
        Author actual = response.body().as(Author.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp.toString(), actual.toString()));
    }

    @Step("Verify delete author")
    public void deleteBookAsserter(Response response) {
        int actual = response.statusCode();
        int exp = HttpStatus.SC_NO_CONTENT;
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }
}
