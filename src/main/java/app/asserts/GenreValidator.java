package app.asserts;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import utils.entities.Genre;

public class GenreValidator {
    public static final String EXPECTED_BUT_FOUND = "Expected [%s], but found [%s]";

    @Step("Verify create genre")
    public void createGenreAsserter(Response response, Genre exp) {
        Assert.assertEquals(response.statusCode(), HttpStatus.SC_CREATED);
        Genre actual = response.body().as(Genre.class);
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }

    @Step("Verify delete genre")
    public void deleteGenreAsserter(Response response) {
        int actual = response.statusCode();
        int exp = HttpStatus.SC_NO_CONTENT;
        Assert.assertEquals(actual, exp,
                String.format(EXPECTED_BUT_FOUND, exp, actual));
    }
}
