package app.services;

import config.PropManager;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.entities.Genre;

public class GenreService {

    @Step("Create genre request")
    public Response createGenre(Genre genre) {
        RestAssured.baseURI = PropManager.getBaseUrl();
        RequestSpecification request = RestAssured.given()
                .body(genre)
                .contentType(ContentType.JSON)
                .basePath("/genre");
        return RestClient.post(request);
    }

    @Step("Delete genre request")
    public Response deleteGenre(Long genreId) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/genre/" + genreId);
        return RestClient.delete(request);
    }
}
