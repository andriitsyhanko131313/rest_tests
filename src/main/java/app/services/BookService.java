package app.services;

import config.PropManager;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.entities.Author;
import utils.entities.Book;
import utils.entities.Genre;

public class BookService {

    @Step("Create book request")
    public Response createBook(Book book, Author author, Genre genre) {
        RestAssured.baseURI = PropManager.getBaseUrl();
        RequestSpecification request = RestAssured.given()
                .body(book)
                .contentType(ContentType.JSON)
                .basePath("/book/" + author.getAuthorId() + "/" + genre.getGenreId());
        return RestClient.post(request);
    }

    @Step("Delete book request")
    public Response deleteBook(Long bookId) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/book/" + bookId);
        return RestClient.delete(request);
    }
}
