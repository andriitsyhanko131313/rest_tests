package app.services;

import config.PropManager;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.entities.Author;
import utils.entities.Book;
import utils.entities.Genre;

public class AuthorService {
    @Step("Create author request")
    public Response createAuthor(Author author) {
        RestAssured.baseURI = PropManager.getBaseUrl();
        RequestSpecification request = RestAssured.given()
                .body(author)
                .contentType(ContentType.JSON)
                .basePath("/author");
        return RestClient.post(request);
    }

    @Step("Get author request")
    public Response getAuthor(Long authorId) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/author/" + authorId);
        return RestClient.get(request);
    }

    @Step("Delete author request")
    public Response deleteAuthor(Long authorId) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/author/" + authorId);
        return RestClient.delete(request);
    }

    @Step("Update author request")
    public Response updateAuthor(Author author) {
        RequestSpecification request = RestAssured.given()
                .body(author)
                .contentType(ContentType.JSON)
                .basePath("/author");
        return RestClient.put(request);
    }

    @Step("Search author by  name request")
    public Response searchAuthorByName(String authorName) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/authors/search")
                .queryParam("query", authorName);
        return RestClient.get(request);
    }

    @Step("Get author by book request")
    public Response getAuthorByBook(Book book) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/book/" + book.getBookId() + "/author");
        return RestClient.get(request);
    }

    @Step("Get all authors in special genre request")
    public Response getAllAuthorsInSpecialGenre(Genre genre) {
        RequestSpecification request = RestAssured.given()
                .accept(ContentType.JSON)
                .basePath("/genre/" + genre.getGenreId() + "/authors");
        return RestClient.get(request);
    }
}
