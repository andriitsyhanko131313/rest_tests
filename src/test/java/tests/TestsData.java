package tests;

import app.asserts.AuthorValidator;
import app.asserts.BookValidator;
import app.asserts.GenreValidator;
import app.services.AuthorService;
import app.services.BookService;
import app.services.GenreService;
import com.google.inject.Inject;
import io.restassured.response.Response;
import listener.RestTestListener;
import org.testng.annotations.Guice;
import org.testng.annotations.Listeners;
import utils.FileManager;
import utils.entities.Author;
import utils.entities.Book;
import utils.entities.Genre;

@Guice
@Listeners(RestTestListener.class)
public class TestsData {

    @Inject
    protected AuthorService authorService;
    @Inject
    protected GenreService genreService;
    @Inject
    protected BookService bookService;
    @Inject
    protected AuthorValidator authorValidator;
    @Inject
    protected BookValidator bookValidator;
    @Inject
    protected GenreValidator genreValidator;

    protected Book validBook = FileManager.getValidBook();
    protected Genre validGenre = FileManager.getValidGenre();
    protected Author validAuthor = FileManager.getValidAuthor();
    protected Author updatedAuthor = FileManager.getUpdatedAuthor();
    protected Response response;
}
