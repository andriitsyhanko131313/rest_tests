package tests;

import org.apache.http.HttpStatus;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.FileManager;
import utils.entities.Author;

import java.io.IOException;


public class AuthorTests extends TestsData {

    @DataProvider
    public Object[] invalidAuthorsList() {
        return FileManager.getInvalidAuthorsList().toArray();
    }

    @DataProvider
    public Object[] invalidAuthors1List() {
        return FileManager.getInvalidAuthors1List().toArray();
    }


    @Test(description = "Create author test case")
    public void createAuthorTestCase() {
        response = authorService.createAuthor(validAuthor);
        authorValidator.createAuthorAsserter(response, validAuthor);
    }

    @Test(description = "Get author test case")
    public void getAuthorTestCase() {
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.getAuthor(validAuthor.getAuthorId());
        authorValidator.getAuthorAsserter(response, validAuthor);
    }

    @Test(description = "Delete author test case")
    public void deleteAuthorTestCase() {
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.deleteAuthor(validAuthor.getAuthorId());
        authorValidator.deleteAuthorAsserter(response);
    }

    @Test(description = "Update author test case")
    public void updateAuthorTestCase() {
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.updateAuthor(updatedAuthor);
        authorValidator.updateAuthorAsserter(response, updatedAuthor);
    }

    @Test(description = "Get authors by name test case")
    public void getAuthorsByNameTestCase() {
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.searchAuthorByName(validAuthor.getAuthorName().getFirst());
        authorValidator.getAuthorsByNameAsserter(response, validAuthor.getAuthorName().getFirst());
    }

    @Test(description = "Get author by book test case")
    public void getAuthorByBookTestCase() {
        genreService.createGenre(validGenre).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        bookService.createBook(validBook, validAuthor, validGenre).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.getAuthorByBook(validBook);
        authorValidator.getAuthorAsserter(response, validAuthor);
    }

    @Test(description = "Get authors by genre test case")
    public void getAuthorsByGenreTestCase() throws IOException {
        genreService.createGenre(validGenre).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        bookService.createBook(validBook, validAuthor, validGenre);
        response = authorService.getAllAuthorsInSpecialGenre(validGenre);
        authorValidator.getAuthorsByGenreAsserter(response, validAuthor);
    }

    @Test(description = "Create author if already exist test case")
    public void createAuthorIfExistTestCaseTestCase() {
        authorService.createAuthor(validAuthor).then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);
        response = authorService.createAuthor(validAuthor);
        authorValidator.statusConflictAsserter(response);
    }

    @Test(description = "Get author if doesn't exist test case")
    public void getAuthorThatDoesNotExistTestCase() {
        response = authorService.getAuthor(validAuthor.getAuthorId());
        authorValidator.statusNotFoundAsserter(response);
    }

    @Test(description = "Update author if doesn't exist test case")
    public void updateAuthorIfDoesNotExistTestCase() {
        response = authorService.updateAuthor(updatedAuthor);
        authorValidator.statusNotFoundAsserter(response);
    }

    @Test(description = "Delete author if doesn't exist test case")
    public void deleteAuthorIfDoesNotExistTestCase() {
        response = authorService.deleteAuthor(validAuthor.getAuthorId());
        authorValidator.statusNotFoundAsserter(response);
    }

    @Test(description = "Create author with empty Id, empty first name, empty second name test case",
            dataProvider = "invalidAuthorsList")
    public void creteAuthorWithEmptyDataTestCase(Author author) {
        response = authorService.createAuthor(author);
        authorValidator.statusBadRequestAsserter(response);
    }

    @AfterMethod(description = "Delete test data")
    public void clearData() {
        bookService.deleteBook(validBook.getBookId());
        authorService.deleteAuthor(validAuthor.getAuthorId());
        genreService.deleteGenre(validGenre.getGenreId());
    }
}