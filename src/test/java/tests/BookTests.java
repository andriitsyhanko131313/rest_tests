package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class BookTests extends TestsData {

    @Test(description = "Create  book test case")
    public void createBookTestCase() {
        authorService.createAuthor(validAuthor);
        genreService.createGenre(validGenre);
        response = bookService.createBook(validBook, validAuthor, validGenre);
        bookValidator.createBookAsserter(response, validBook);
    }

    @Test(description = "Delete book test case")
    public void deleteBookTestCase() {
        authorService.createAuthor(validAuthor);
        genreService.createGenre(validGenre);
        bookService.createBook(validBook, validAuthor, validGenre);
        response = bookService.deleteBook(validBook.getBookId());
        bookValidator.deleteBookAsserter(response);
    }


    @AfterMethod(description = "Delete test data")
    public void clearData() {
        authorService.deleteAuthor(validAuthor.getAuthorId());
        bookService.deleteBook(validBook.getBookId());
        genreService.deleteGenre(validGenre.getGenreId());
    }
}
