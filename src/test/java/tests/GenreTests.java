package tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class GenreTests extends TestsData {

    @Test(description = "Create genre test case")
    public void creteGenreTestCase() {
        response = genreService.createGenre(validGenre);
        genreValidator.createGenreAsserter(response, validGenre);
    }

    @Test(description = "Delete genre test case")
    public void deleteGenreTestCase() {
        genreService.createGenre(validGenre);
        response = genreService.deleteGenre(validGenre.getGenreId());
        genreValidator.deleteGenreAsserter(response);
    }

    @AfterMethod(description = "Delete test data")
    public void clearData() {
        genreService.deleteGenre(validGenre.getGenreId());
    }

}
